# Git - The Simple Guide

just a simple guide for getting started with git. no deep shit ;)

## Getting Started

Git is a version control system for tracking changes in computer files and coordinating work on those files among multiple people

### Setup

All things you need just download and install the software from

```
https://git-scm.com/downloads
```

### Git global setup

```
git config --global user.name "higahiga"
git config --global user.email "higahiga@example.com"
```

## Working with directory

You’ve now initialized the working directory—​you may notice a new directory created, named ".git".

### Create a new repository

```
git clone @server_name.git
cd project_name
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

### Existing folder

```
cd existing_folder
git init
git remote add origin @server_name.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

### Existing Git repository

```
cd existing_repo
git remote rename origin old-origin
git remote add origin @server_name.git
git push -u origin --all
git push -u origin --tags
```

## Working with synchronize

Feature Synchronize workflow

```
git remote add origin @server_name.git
git checkout master 
git pull 
git checkout --$restore_filename
git rm delete_filename
git commit -m "Commit message" 
git push origin master 
```

| Command | EXPLAIN |
| ------ | ------ |
| git remote add origin @server_name.git | connect local repository to remote repository  |
| git checkout master  | switch to master branch  |
| git pull | update local repository with remote changes  |
| git checkout --$restore_filename | replace working copy with latest from HEAD git checkout |
| git rm @delete_filename | remove/delete file |
| git push origin master  | push changes to remote repository |


## Working with branch

Feature branch workflow

```
git branch -d $feature_branch
git checkout -b $feature_branch 
git commit -am "My feature is ready"
git push origin $feature_branch 
```

## Tagging & Log

It's recommended to create tags for software releases

    git tag $tag_name $commit_ID 

 You can study repository history using.. `git log`
 
```
git log --author=bob
git log --pretty=oneline
git log --graph --oneline --decorate --all
git log --name_status
git log --help
```

## Useful hints, links & resources

